package com.example.mtec

import android.app.ProgressDialog
import android.bluetooth.BluetoothAdapter
import android.bluetooth.BluetoothDevice
import android.bluetooth.BluetoothSocket
import android.content.Context
import android.os.AsyncTask
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import java.io.IOException
import java.util.*

class valve8_control : AppCompatActivity() {
    companion object {
        var m_myUUID: UUID = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB")
        var m_bluetoothSocket: BluetoothSocket? = null
        lateinit var m_progress: ProgressDialog
        lateinit var m_bluetoothAdapter: BluetoothAdapter
        var m_isConnected: Boolean = false
        var m_address: String? = null
    }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_valve8_control)
        valve8_control.m_address = intent.getStringExtra(MainActivity.EXTRA_ADDRESS).toString()
        valve8_control.ConnectToDevice(this).execute()









    }

    private fun sendCommand(input: String)  {  //เอา
        if (valve4_control.m_bluetoothSocket != null) {
            try{

                valve4_control.m_bluetoothSocket!!.outputStream.write(input.toByteArray())
                valve4_control.m_bluetoothSocket!!.outputStream.flush()


            } catch (e: IOException) {
                e.printStackTrace()
            }
        }

    }


    private fun disconnect() { //disconnect bluetooth
        if (valve4_control.m_bluetoothSocket != null) {
            try {
                valve4_control.m_bluetoothSocket!!.close()
                valve4_control.m_bluetoothSocket = null
                valve4_control.m_isConnected = false
            } catch (e: IOException) {
                e.printStackTrace()
            }
        }
        finish()
    }
    private class ConnectToDevice(c: Context) : AsyncTask<Void, Void, String>() {
        private var connectSuccess: Boolean = true
        private val context: Context

        init {
            this.context = c
        }

        override fun onPreExecute() {
            super.onPreExecute()
            valve4_control.m_progress = ProgressDialog.show(context, "Connecting...", "please wait")
        }

        override fun doInBackground(vararg p0: Void?): String? {
            try {
                if (valve4_control.m_bluetoothSocket == null || !valve4_control.m_isConnected) {
                    valve4_control.m_bluetoothAdapter = BluetoothAdapter.getDefaultAdapter()
                    val device: BluetoothDevice = valve4_control.m_bluetoothAdapter.getRemoteDevice(valve4_control.m_address)
                    valve4_control.m_bluetoothSocket = device.createInsecureRfcommSocketToServiceRecord(valve4_control.m_myUUID)
                    BluetoothAdapter.getDefaultAdapter().cancelDiscovery()
                    valve4_control.m_bluetoothSocket!!.connect()
                }
            } catch (e: IOException) {
                connectSuccess = false
                e.printStackTrace()
            }
            return null
        }

        override fun onPostExecute(result: String?) {
            super.onPostExecute(result)
            if (!connectSuccess) {
                Log.i("data", "couldn't connect")
            } else {
                valve4_control.m_isConnected = true
            }
            valve4_control.m_progress.dismiss()
        }
    }

}
package com.example.mtec

import android.app.Activity
import android.app.AlertDialog
import android.bluetooth.BluetoothAdapter
import android.bluetooth.BluetoothDevice
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.example.mtec.valve4_control.Companion.m_address
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.bluetooth_list.*
import kotlinx.android.synthetic.main.bluetooth_list.view.*

class MainActivity : AppCompatActivity() {
    private var m_bluetoothAdapter: BluetoothAdapter? = null
    private lateinit var m_pairedDevices: Set<BluetoothDevice>
    private val REQUEST_ENABLE_BLUETOOTH = 1
    private var mDialogView: View? = null


    companion object {
        val EXTRA_ADDRESS: String = "Device_address"

    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_main)
        m_bluetoothAdapter = BluetoothAdapter.getDefaultAdapter()


        if (m_bluetoothAdapter == null) {
            Toast.makeText(this, "this device doesn't support bluetooth", Toast.LENGTH_SHORT).show()
            return
        }
        if (!m_bluetoothAdapter!!.isEnabled) {
            val enableBluetoothIntent = Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE)
            startActivityForResult(enableBluetoothIntent, REQUEST_ENABLE_BLUETOOTH)

        }



        // select_device_refresh.setOnClickListener{ pairedDeviceList() }
        bt4valve.setOnTouchListener { v, event ->
            when (event.action) {
                MotionEvent.ACTION_DOWN -> {
                    //bt4valve.setImageResource(R.drawable.valve4_on)

                    bt4valve.setBackgroundResource(R.drawable.valve8_on)
                    true
                }
                MotionEvent.ACTION_UP -> {
                    v.performClick()
                    bt4valve.setBackgroundResource(R.drawable.valve8_off)
                    openDevicedialog()
                    true
                }
                else -> false
            }
        }
       /*bt8valve.setOnTouchListener { v, event ->
            when (event.action) {
                MotionEvent.ACTION_DOWN -> {
                    //bt4valve.setImageResource(R.drawable.valve4_on)
                    bt8valve.setBackgroundResource(R.drawable.valve8_on)
                    true
                }
                MotionEvent.ACTION_UP -> {
                    v.performClick()
                    bt8valve.setBackgroundResource(R.drawable.valve8_off)

                    true
                }
                else -> false
            }
        }*/

    }

    private fun openDevicedialog() {
        mDialogView = LayoutInflater.from(this).inflate(R.layout.bluetooth_list, null);
        val mBuilder = AlertDialog.Builder(this)
            .setView(mDialogView)
            .setTitle("Bluetooth list")
        val mAlertDialog = mBuilder.show()

        mDialogView?.select_device_refresh?.setOnClickListener {
            pairedDeviceList()
            //เมื่อกดปุ่ม refresh จะออกจากโปรแกรม ไม่แน่ใจว่าพลาดตรงไหน อันนี้ผมทำเป็น dialog ปกติที่คิดไว้เมื่อกดเลือกอุปกรณ์จาก list view จะเข้าสู่หน้า mainactivity เพื่อจะไปเลือกโหมด 3 โหมด
            //และมี led status ของ bluetooth สัญญลักษณ์มุมขวาจะเรืองแสงใน drawable
        }
        mDialogView?.cancel_bluetooth_list?.setOnClickListener {
            mAlertDialog.dismiss() // ทำงานได้ปกติ
        }
    }



    private fun pairedDeviceList() {
        m_pairedDevices = m_bluetoothAdapter!!.bondedDevices
        val list: ArrayList<BluetoothDevice> = ArrayList()

        if (m_pairedDevices.isNotEmpty()) {
            for (device: BluetoothDevice in m_pairedDevices) {
                list.add(device)
                Log.i("device", "" + device)
            }
        } else {
            Toast.makeText(this, "no paired bluetooth devices found", Toast.LENGTH_SHORT).show()
            //เพิ่มไดอลอกแจ้งให้เชื่อม ble
        }

        val adapter = ArrayAdapter(this, android.R.layout.simple_list_item_1, list)

        mDialogView?.select_device_list?.adapter = adapter
        mDialogView?.select_device_list?.onItemClickListener = AdapterView.OnItemClickListener { _, _, position, _ ->
            val device: BluetoothDevice = list[position]
            val address: String = device.address

            val intent = Intent(this, valve4_control::class.java)
            intent.putExtra(EXTRA_ADDRESS, address)
            startActivity(intent)
        }

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == REQUEST_ENABLE_BLUETOOTH) {
            if (resultCode == Activity.RESULT_OK) {
                if (m_bluetoothAdapter!!.isEnabled) {
                    Toast.makeText(this, "Bluetooth has been enabled", Toast.LENGTH_SHORT).show()
                } else {
                    Toast.makeText(this, "Bluetooth has been disabled", Toast.LENGTH_SHORT).show()
                }
            } else if (resultCode == Activity.RESULT_CANCELED) {
                Toast.makeText(this, "Bluetooth enabling has been canceled", Toast.LENGTH_SHORT).show()
            }
        }
    }
    
}




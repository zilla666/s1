package com.example.mtec


import android.app.ProgressDialog
import android.bluetooth.BluetoothAdapter
import android.bluetooth.BluetoothDevice
import android.bluetooth.BluetoothSocket
import android.content.Context
import android.os.AsyncTask
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.MotionEvent
import android.view.View
import android.view.ViewConfiguration
import android.widget.Switch
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.ViewConfigurationCompat
import androidx.core.view.postDelayed
import kotlinx.android.synthetic.main.activity_valve4_control.*
import java.io.IOException
import java.util.*
import kotlin.concurrent.schedule
import kotlin.system.measureNanoTime


//import android.os.Handler as Handler1


class valve4_control<MapView> : AppCompatActivity() {

    companion object {
        var m_myUUID: UUID = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB")
        var m_bluetoothSocket: BluetoothSocket? = null
        lateinit var m_progress: ProgressDialog
        lateinit var m_bluetoothAdapter: BluetoothAdapter
        var m_isConnected: Boolean = false
         var m_address: String? = null
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_valve4_control)

        m_address = intent.getStringExtra(MainActivity.EXTRA_ADDRESS).toString()
        ConnectToDevice(this).execute()


        save1.setOnTouchListener { v, event ->
            when(event.action){
                MotionEvent.ACTION_DOWN -> {
                    Toast.makeText(this, "save pressure position 1", Toast.LENGTH_SHORT).show()
                    sendCommand("d")

                    true
                }
                MotionEvent.ACTION_UP -> {
                    v.performClick()

                    //set1.setBackgroundResource(R.drawable.no1_on)
                    true
                }
                else -> false
            }
        }
        save2.setOnTouchListener { v, event ->
            when(event.action){
                MotionEvent.ACTION_DOWN -> {
                    Toast.makeText(this, "save pressure position 2", Toast.LENGTH_SHORT).show()
                    sendCommand("e")

                    true
                }
                MotionEvent.ACTION_UP -> {
                    v.performClick()

                    //set1.setBackgroundResource(R.drawable.no1_on)
                    true
                }
                else -> false
            }
        }
        save3.setOnTouchListener { v, event ->
            when(event.action){
                MotionEvent.ACTION_DOWN -> {
                    Toast.makeText(this, "save pressure position 3", Toast.LENGTH_SHORT).show()
                    sendCommand("f")

                    true
                }
                MotionEvent.ACTION_UP -> {
                    v.performClick()

                    //set1.setBackgroundResource(R.drawable.no1_on)
                    true
                }
                else -> false
            }
        }

        set1.setOnTouchListener { v, event ->
            when(event.action){
                MotionEvent.ACTION_DOWN -> {
                    set1.setBackgroundResource(R.drawable.no1_on)
                    set2.setBackgroundResource(R.drawable.no2_off)
                    set3.setBackgroundResource(R.drawable.no3_off)
                    sendCommand("g")

                    true
                }
                MotionEvent.ACTION_UP -> {
                    v.performClick()

                    set1.setBackgroundResource(R.drawable.no1_on)
                    true
                }
                else -> false
            }
        }

        set2.setOnTouchListener { v, event ->
            when(event.action){
                MotionEvent.ACTION_DOWN -> {
                    set2.setBackgroundResource(R.drawable.no2_on)
                    set1.setBackgroundResource(R.drawable.no1_off)
                    set3.setBackgroundResource(R.drawable.no3_off)
                    sendCommand("h")
                    true
                }
                MotionEvent.ACTION_UP -> {
                    v.performClick()

                    set2.setBackgroundResource(R.drawable.no2_on)
                    true
                }
                else -> false
            }
        }
        set3.setOnTouchListener { v, event ->
            when(event.action){
                MotionEvent.ACTION_DOWN -> {
                    set3.setBackgroundResource(R.drawable.no3_on)
                    set1.setBackgroundResource(R.drawable.no1_off)
                    set2.setBackgroundResource(R.drawable.no2_off)
                    sendCommand("i")
                    true
                }
                MotionEvent.ACTION_UP -> {
                    v.performClick()

                    set3.setBackgroundResource(R.drawable.no3_on)
                    true
                }
                else -> false
            }
        }


       val repeater: Runnable = object : Runnable { //edit repeat1..2
            override fun run() {
                up4wheel(); //edit
                btn_up4wheel.postDelayed(this, 0) //edit
                btn_up4wheel.post(this)
            }
        }

        btn_up4wheel.setOnTouchListener { v, event -> // ส่งข้อมูลไป arduino
           when (event.action) {
               MotionEvent.ACTION_DOWN-> {
                   btn_up4wheel.setBackgroundResource(R.drawable.upall_on)
                   //btn_up4wheel.postDelayed(repeater, ViewConfiguration.getLongPressTimeout().toLong()) //add
                   up4wheel()
                   set1.setBackgroundResource(R.drawable.no1_off)
                   set2.setBackgroundResource(R.drawable.no2_off)
                   set3.setBackgroundResource(R.drawable.no3_off)
                   Timer().schedule(0) {
                       v.postDelayed(repeater, ViewConfiguration.getLongPressTimeout().toLong()) //add
                       up4wheel()
                   }
                   true
               }
               MotionEvent.ACTION_UP -> {
                   btn_up4wheel.performClick()
                   btn_up4wheel.removeCallbacks(repeater)//add
                   btn_up4wheel.setBackgroundResource(R.drawable.upall_off)
                   true
               }
                else -> false
            }
        }

        val repeater1: Runnable = object : Runnable { //edit repeat1..2
            override fun run() {
                up_front(); //edit
                btn_up_front.postDelayed(this, 1) //edit
            }
        }
        btn_up_front.setOnTouchListener { v, event -> // ส่งข้อมูลไป arduino
            when (event.action) {
                MotionEvent.ACTION_DOWN -> {
                    set1.setBackgroundResource(R.drawable.no1_off)
                    set2.setBackgroundResource(R.drawable.no2_off)
                    set3.setBackgroundResource(R.drawable.no3_off)
                    btn_up_front.setBackgroundResource(R.drawable.main_up_on)
                    up_front(); //edit
                    v.postDelayed(repeater1, ViewConfiguration.getLongPressTimeout().toLong());
                    true
                }
                MotionEvent.ACTION_UP -> {
                    v.performClick()
                    v.removeCallbacks(repeater1)
                    btn_up_front.setBackgroundResource(R.drawable.main_up_off)
                    true
                }
                else -> false
            }
        }

        val repeater2: Runnable = object : Runnable { //edit repeat1..
            override fun run() {
                dn_front(); //edit
                btn_dn_front.postDelayed(this, 1) //edit
            }
        }
        btn_dn_front.setOnTouchListener { v, event -> // ส่งข้อมูลไป arduino
            when (event.action) {
                MotionEvent.ACTION_DOWN -> {
                    set1.setBackgroundResource(R.drawable.no1_off)
                    set2.setBackgroundResource(R.drawable.no2_off)
                    set3.setBackgroundResource(R.drawable.no3_off)
                    btn_dn_front.setBackgroundResource(R.drawable.main_down_on)
                    dn_front(); //edit
                    v.postDelayed(repeater2, ViewConfiguration.getLongPressTimeout().toLong());
                    true
                }
                MotionEvent.ACTION_UP -> {
                    v.performClick()
                    v.removeCallbacks(repeater2)
                    btn_dn_front.setBackgroundResource(R.drawable.main_down_off)
                    true
                }
                else -> false
            }
        }
        val repeater3: Runnable = object : Runnable { //edit repeat1..
            override fun run() {
                up_rear(); //edit
                btn_up_rear.postDelayed(this, 1) //edit
            }
        }
        btn_up_rear.setOnTouchListener { v, event -> // ส่งข้อมูลไป arduino
            when (event.action) {
                MotionEvent.ACTION_DOWN -> {
                    set1.setBackgroundResource(R.drawable.no1_off)
                    set2.setBackgroundResource(R.drawable.no2_off)
                    set3.setBackgroundResource(R.drawable.no3_off)
                    btn_up_rear.setBackgroundResource(R.drawable.main_up_on)
                    up_rear()
                    v.postDelayed(repeater3, ViewConfiguration.getLongPressTimeout().toLong());
                    //sendCommand("E")
                    true
                }
                MotionEvent.ACTION_UP -> {
                    v.performClick()
                    v.removeCallbacks(repeater3)
                    btn_up_rear.setBackgroundResource(R.drawable.main_up_off)
                    true
                }
                else -> false
            }
        }
        val repeater4: Runnable = object : Runnable { //edit repeat1..
            override fun run() {
                dn_rear(); //edit
                btn_dn_rear.postDelayed(this, 1) //edit
            }
        }
        btn_dn_rear.setOnTouchListener { v, event -> // ส่งข้อมูลไป arduino
            when (event.action) {
                MotionEvent.ACTION_DOWN -> {
                    set1.setBackgroundResource(R.drawable.no1_off)
                    set2.setBackgroundResource(R.drawable.no2_off)
                    set3.setBackgroundResource(R.drawable.no3_off)
                    btn_dn_rear.setBackgroundResource(R.drawable.main_down_on)
                    dn_rear()
                    v.postDelayed(repeater4, ViewConfiguration.getLongPressTimeout().toLong());
                    true
                }
                MotionEvent.ACTION_UP -> {
                    v.performClick()
                    v.removeCallbacks(repeater4)
                    btn_dn_rear.setBackgroundResource(R.drawable.main_down_off)
                    true
                }
                else -> false
            }
        }

        val repeater5: Runnable = object : Runnable { //edit repeat1..
            override fun run() {
               dn4wheel(); //edit
                btn_dn4wheel.postDelayed(this, 1) //edit
            }
        }
        btn_dn4wheel.setOnTouchListener { v, event -> // ส่งข้อมูลไป arduino
            when (event.action) {
                MotionEvent.ACTION_DOWN -> {
                    set1.setBackgroundResource(R.drawable.no1_off)
                    set2.setBackgroundResource(R.drawable.no2_off)
                    set3.setBackgroundResource(R.drawable.no3_off)
                    btn_dn4wheel.setBackgroundResource(R.drawable.downall_on) //edit pic
                    dn4wheel()
                    v.postDelayed(repeater5, ViewConfiguration.getLongPressTimeout().toLong()); //edit repeater...?
                    //sendCommand("6")
                    true
                }
                MotionEvent.ACTION_UP -> {
                    v.performClick()
                    v.removeCallbacks(repeater5) //repeat >>> recallback
                    btn_dn4wheel.setBackgroundResource(R.drawable.downall_off) //edit pic>>>
                    true
                }
                else -> false
            }
        }
        val repeater6: Runnable = object : Runnable { //edit repeat>>..
            override fun run() {
                up_front_left(); //edit
                btn_up_front_left.postDelayed(this, 1) //edit
            }
        }

        btn_up_front_left.setOnTouchListener { v, event -> // ส่งข้อมูลไป arduino
            when (event.action) {
                MotionEvent.ACTION_DOWN -> {
                    set1.setBackgroundResource(R.drawable.no1_off)
                    set2.setBackgroundResource(R.drawable.no2_off)
                    set3.setBackgroundResource(R.drawable.no3_off)
                    btn_up_front_left.setBackgroundResource(R.drawable.sup_up_on) //edit pic
                    up_front_left()
                    v.postDelayed(repeater6, ViewConfiguration.getLongPressTimeout().toLong()); //edit repeater...?
                    true
                }
                MotionEvent.ACTION_UP -> {
                    v.performClick()
                    v.removeCallbacks(repeater6) //repeat >>> recallback
                    btn_up_front_left.setBackgroundResource(R.drawable.sup_up_off) //edit pic>>>
                    true
                }
                else -> false
            }
        }

        ///////////////////////////repeat 7 !!!!!!!!!!!!!!!!!!!!! E

        val repeater7: Runnable = object : Runnable { //edit repeat>>..
            override fun run() {
                up_front_right(); //edit
                btn_up_front_right.postDelayed(this, 1) //edit
            }
        }

        btn_up_front_right.setOnTouchListener { v, event -> // ส่งข้อมูลไป arduino
            when (event.action) {
                MotionEvent.ACTION_DOWN -> {
                    set1.setBackgroundResource(R.drawable.no1_off)
                    set2.setBackgroundResource(R.drawable.no2_off)
                    set3.setBackgroundResource(R.drawable.no3_off)
                    btn_up_front_right.setBackgroundResource(R.drawable.sup_up_on) //edit pic
                    up_front_right()
                    v.postDelayed(repeater7, ViewConfiguration.getLongPressTimeout().toLong()); //edit repeater...?
                    true
                }
                MotionEvent.ACTION_UP -> {
                    v.performClick()
                    v.removeCallbacks(repeater7) //repeat >>> recallback
                    btn_up_front_right.setBackgroundResource(R.drawable.sup_up_off) //edit pic>>>
                    true
                }
                else -> false
            }
        }

        ///////////////////////////repeat 8 !!!!!!!!!!!!!!!!!!!!!
        val repeater8: Runnable = object : Runnable { //edit repeat>>..
            override fun run() {
                dn_front_left(); //edit
                btn_dn_front_left.postDelayed(this, 1) //edit
            }
        }

        btn_dn_front_left.setOnTouchListener { v, event -> // ส่งข้อมูลไป arduino
            when (event.action) {
                MotionEvent.ACTION_DOWN -> {
                    set1.setBackgroundResource(R.drawable.no1_off)
                    set2.setBackgroundResource(R.drawable.no2_off)
                    set3.setBackgroundResource(R.drawable.no3_off)
                    btn_dn_front_left.setBackgroundResource(R.drawable.sup_dn_on) //edit pic
                    dn_front_left()
                    v.postDelayed(repeater8, ViewConfiguration.getLongPressTimeout().toLong()); //edit repeater...?
                    true
                }
                MotionEvent.ACTION_UP -> {
                    v.performClick()
                    v.removeCallbacks(repeater8) //repeat >>> recallback
                    btn_dn_front_left.setBackgroundResource(R.drawable.sup_dn_off) //edit pic>>>
                    true
                }
                else -> false
            }
        }

        ///////////////////////////repeat 9 !!!!!!!!!!!!!!!!!!!!!
        val repeater9: Runnable = object : Runnable { //edit repeat>>..
            override fun run() {
                dn_front_right(); //edit
                btn_dn_front_right.postDelayed(this, 1) //edit
            }
        }

        btn_dn_front_right.setOnTouchListener { v, event -> // ส่งข้อมูลไป arduino
            when (event.action) {
                MotionEvent.ACTION_DOWN -> {
                    set1.setBackgroundResource(R.drawable.no1_off)
                    set2.setBackgroundResource(R.drawable.no2_off)
                    set3.setBackgroundResource(R.drawable.no3_off)
                    btn_dn_front_right.setBackgroundResource(R.drawable.sup_dn_on) //edit pic
                    dn_front_right()
                    v.postDelayed(repeater9, ViewConfiguration.getLongPressTimeout().toLong()); //edit repeater...?
                    true
                }
                MotionEvent.ACTION_UP -> {
                    v.performClick()
                    v.removeCallbacks(repeater9) //repeat >>> recallback
                    btn_dn_front_right.setBackgroundResource(R.drawable.sup_dn_off) //edit pic>>>
                    true
                }
                else -> false
            }
        }

        ///////////////////////////repeat 10 !!!!!!!!!!!!!!!!!!!!!
        val repeater10: Runnable = object : Runnable { //edit repeat>>..
            override fun run() {
                 dn_rear_left(); //edit
                btn_dn_rear_right.postDelayed(this, 1) //edit
            }
        }

        btn_dn_rear_right.setOnTouchListener { v, event -> // ส่งข้อมูลไป arduino
            when (event.action) {
                MotionEvent.ACTION_DOWN -> {
                    set1.setBackgroundResource(R.drawable.no1_off)
                    set2.setBackgroundResource(R.drawable.no2_off)
                    set3.setBackgroundResource(R.drawable.no3_off)
                    btn_dn_rear_right.setBackgroundResource(R.drawable.sup_dn_on) //edit pic
                    dn_rear_left()
                    v.postDelayed(repeater10, ViewConfiguration.getLongPressTimeout().toLong()); //edit repeater...?
                    true
                }
                MotionEvent.ACTION_UP -> {
                    v.performClick()
                    v.removeCallbacks(repeater10) //repeat >>> recallback
                    btn_dn_rear_right.setBackgroundResource(R.drawable.sup_dn_off) //edit pic>>>
                    true
                }
                else -> false
            }
        }
        ///////////////////////////repeat 11 !!!!!!!!!!!!!!!!!!!!!
        val repeater11: Runnable = object : Runnable { //edit repeat>>..
            override fun run() {
                up_rear_left(); //edit
                btn_up_rear_left.postDelayed(this, 1) //edit
            }
        }


        btn_up_rear_left.setOnTouchListener { v, event -> // ส่งข้อมูลไป arduino
            when (event.action) {
                MotionEvent.ACTION_DOWN -> {
                    set1.setBackgroundResource(R.drawable.no1_off)
                    set2.setBackgroundResource(R.drawable.no2_off)
                    set3.setBackgroundResource(R.drawable.no3_off)
                    btn_up_rear_left.setBackgroundResource(R.drawable.sup_up_on) //edit pic
                    up_rear_left()
                    v.postDelayed(repeater11, ViewConfiguration.getLongPressTimeout().toLong()); //edit repeater...?
                    true
                }
                MotionEvent.ACTION_UP -> {
                    v.performClick()
                    v.removeCallbacks(repeater11) //repeat >>> recallback
                    btn_up_rear_left.setBackgroundResource(R.drawable.sup_up_off) //edit pic>>>
                    true
                }
                else -> false
            }
        }

        ///////////////////////////repeat 12 !!!!!!!!!!!!!!!!!!!!!
        val repeater12: Runnable = object : Runnable { //edit repeat>>..
            override fun run() {
                up_rear_right(); //edit
                btn_up_rear_right.postDelayed(this, 1) //edit
            }
        }


        btn_up_rear_right.setOnTouchListener { v, event -> // ส่งข้อมูลไป arduino
            when (event.action) {
                MotionEvent.ACTION_DOWN -> {
                    set1.setBackgroundResource(R.drawable.no1_off)
                    set2.setBackgroundResource(R.drawable.no2_off)
                    set3.setBackgroundResource(R.drawable.no3_off)
                    btn_up_rear_right.setBackgroundResource(R.drawable.sup_up_on) //edit pic
                    up_rear_right()
                    v.postDelayed(repeater12, ViewConfiguration.getLongPressTimeout().toLong()); //edit repeater...?
                    true
                }
                MotionEvent.ACTION_UP -> {
                    v.performClick()
                    v.removeCallbacks(repeater12) //repeat >>> recallback
                    btn_up_rear_right.setBackgroundResource(R.drawable.sup_up_off) //edit pic>>>
                    true
                }
                else -> false
            }
        }
        ///////////////////////////repeat 13 !!!!!!!!!!!!!!!!!!!!!
        val repeater13: Runnable = object : Runnable { //edit repeat>>..
            override fun run() {
                dn_rear_left(); //edit
                btn_dn_rear_left.postDelayed(this, 1) //edit
            }
        }

        btn_dn_rear_left.setOnTouchListener { v, event -> // ส่งข้อมูลไป arduino
            when (event.action) {
                MotionEvent.ACTION_DOWN -> {
                    set1.setBackgroundResource(R.drawable.no1_off)
                    set2.setBackgroundResource(R.drawable.no2_off)
                    set3.setBackgroundResource(R.drawable.no3_off)
                    btn_dn_rear_left.setBackgroundResource(R.drawable.sup_dn_on) //edit pic
                    dn_rear_left()
                    v.postDelayed(repeater13, ViewConfiguration.getLongPressTimeout().toLong()); //edit repeater...?
                    true
                }
                MotionEvent.ACTION_UP -> {
                    v.performClick()
                    v.removeCallbacks(repeater13) //repeat >>> recallback
                    btn_dn_rear_left.setBackgroundResource(R.drawable.sup_dn_off) //edit pic>>>
                    true
                }
                else -> false
            }
        }
        ///////////////////////////repeat 14 !!!!!!!!!!!!!!!!!!!!!
        val repeater14: Runnable = object : Runnable { //edit repeat>>..
            override fun run() {
                dn_rear_right(); //edit
                btn_dn_rear_right.postDelayed(this, 1) //edit
            }
        }

        btn_dn_rear_right.setOnTouchListener { v, event -> // ส่งข้อมูลไป arduino
            when (event.action) {
                MotionEvent.ACTION_DOWN -> {
                    set1.setBackgroundResource(R.drawable.no1_off)
                    set2.setBackgroundResource(R.drawable.no2_off)
                    set3.setBackgroundResource(R.drawable.no3_off)
                    btn_dn_rear_right.setBackgroundResource(R.drawable.sup_dn_on) //edit pic
                    dn_rear_right()
                    v.postDelayed(repeater14, ViewConfiguration.getLongPressTimeout().toLong()); //edit repeater...?
                    true
                }
                MotionEvent.ACTION_UP -> {
                    v.performClick()
                    v.removeCallbacks(repeater14) //repeat >>> recallback
                    btn_dn_rear_right.setBackgroundResource(R.drawable.sup_dn_off) //edit pic>>>
                    true
                }
                else -> false
            }
        }



        }



    private fun sendCommand(input: String)  {  //เอา
       if (m_bluetoothSocket != null) {
           try{

                m_bluetoothSocket!!.outputStream.write(input.toByteArray())
               m_bluetoothSocket!!.outputStream.flush()


           } catch (e: IOException) {
               e.printStackTrace()
           }
       }

   }


    private fun disconnect() { //disconnect bluetooth
        if (m_bluetoothSocket != null) {
            try {
                m_bluetoothSocket!!.close()
                m_bluetoothSocket = null
                m_isConnected = false
            } catch (e: IOException) {
                e.printStackTrace()
            }
        }
        finish()
    }
    private class ConnectToDevice(c: Context) : AsyncTask<Void, Void, String>() {
        private var connectSuccess: Boolean = true
        private val context: Context

        init {
            this.context = c
        }

        override fun onPreExecute() {
            super.onPreExecute()
            m_progress = ProgressDialog.show(context, "Connecting...", "please wait")
        }

        override fun doInBackground(vararg p0: Void?): String? {
            try {
                if (m_bluetoothSocket == null || !m_isConnected) {
                    m_bluetoothAdapter = BluetoothAdapter.getDefaultAdapter()
                    val device: BluetoothDevice = m_bluetoothAdapter.getRemoteDevice(m_address)
                    m_bluetoothSocket = device.createInsecureRfcommSocketToServiceRecord(m_myUUID)
                    BluetoothAdapter.getDefaultAdapter().cancelDiscovery()
                    m_bluetoothSocket!!.connect()
                }
            } catch (e: IOException) {
                connectSuccess = false
                e.printStackTrace()
            }
            return null
        }

        override fun onPostExecute(result: String?) {
            super.onPostExecute(result)
            if (!connectSuccess) {
                Log.i("data", "couldn't connect")
            } else {
                m_isConnected = true
            }
            m_progress.dismiss()
        }
    }

//Front
    private fun up_front() {
sendCommand("3")
    }
    private fun up_front_left() {//ขึ้นซ้าย
        sendCommand("2")
    }

    private fun dn_front_left(){//ลงซ้าย
        sendCommand("5")
    }

    private fun up_front_right(){//ลงซ้าย
        sendCommand("E")
    }
    private fun dn_front_right(){//ลงซ้าย
        sendCommand("F")
    }
    private fun dn_front(){//ลงซ้าย
        sendCommand("6")
    }

/////////////////////////////////////////
//Rear
private fun up_rear() {
    sendCommand("9")
}
    private fun up_rear_left() {//ขึ้นซ้าย
        sendCommand("8")
    }

    private fun dn_rear_left(){//ลงซ้าย
        sendCommand("0")
    }

    private fun up_rear_right(){//ลงซ้าย
        sendCommand("G")
    }
    private fun dn_rear_right(){//ลงซ้าย
        sendCommand("H")
    }
    private fun dn_rear(){//ลงซ้าย
        sendCommand("I")
    }
    ////////////////////////////////////
    private fun up4wheel(){//ลงซ้าย
        sendCommand("7")
    }
    private fun dn4wheel(){//ลงซ้าย
        sendCommand("D")
    }
//test gitcxdf

    }













